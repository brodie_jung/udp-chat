#ifndef _SENDER_H_
#define _SENDER_H_

#include "list.h"
#include <pthread.h>
#include <stdbool.h> 
#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <netdb.h>
#include <sys/socket.h>

void Sender_msg_sender_consumer_init(List* outputMsgList, int sockDescriptor,
                                                char* hostName, char* remotePortNum);
void Sender_shutdown();



#endif