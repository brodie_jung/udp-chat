#include "sender.h"
#include "input.h"      //since I need the MAX_NUM_STR

static pthread_t pthreadMsgSender;
static List* outgoingMsgList;
static char* remotePort;
static int socketDescriptor;
static char* remoteHostName;

//Sends messages
void* Sender_msg_sender_consumer(void* unused){
    struct addrinfo *results;
    //Opens up socket to send messages
    int msgReceiver = getaddrinfo(remoteHostName, remotePort, NULL, &results);

    if(msgReceiver != 0){
        perror("Failed to getaddrinfo.");
        exit(EXIT_FAILURE);
    }

    char* newTextToSend;
    while(1){  
        //Gets the message from the list to send
        //Critical section is in input.c
        newTextToSend = Input_nextMsg();  
        //Once we get a message send it
        sendto(socketDescriptor, 
                    newTextToSend, MAX_LEN_STR, 0, 
                    (struct sockaddr *) results->ai_addr, results->ai_addrlen);
    }

    free(newTextToSend);

}

//Shuts down the thread
void Sender_shutdown()
{
    pthread_cancel(pthreadMsgSender);
    pthread_join(pthreadMsgSender, NULL);
}

//Takes all the info given and starts the message consumer that sends messages when they appear on the list
void Sender_msg_sender_consumer_init(List* pOutputMsgList, int sockDescriptor, 
                                                char* hostName, char* remotePortNum){
    outgoingMsgList = pOutputMsgList;
    remotePort = remotePortNum;
    remoteHostName = hostName;
    socketDescriptor = sockDescriptor;
    pthread_create(&pthreadMsgSender, NULL, Sender_msg_sender_consumer, NULL);
}