#include <pthread.h>
#include "list.h"
#include <stdio.h>
#include <string.h>
#include "receiver.h"
#include "output.h"

static pthread_t threadOutput;
static char* message;
static List* list;

pthread_cond_t s_syncOkToPrintCond = PTHREAD_COND_INITIALIZER;
pthread_mutex_t s_syncOkToPrintMutex = PTHREAD_MUTEX_INITIALIZER;  

void Output_init(List* receive_list)
{  
    list = receive_list;
    pthread_create(&threadOutput, NULL, Output_outputThread, NULL);
}

//Critical section that appends the given message to the list
void Output_nextMessage(char* messageRx)
{
    pthread_mutex_lock(&s_syncOkToPrintMutex);
    {
        //If we have no more space on the list wait
        if(List_count(list) == LIST_MAX_NUM_NODES){
            pthread_cond_wait(&s_syncOkToPrintCond, &s_syncOkToPrintMutex);
        }
        //Appends to end of list
        List_append(list,messageRx);
        List_first(list);
        //Signal blocking calls (Lets output thread know there is a new message if it was blocked)
        pthread_cond_signal(&s_syncOkToPrintCond);
    }
    pthread_mutex_unlock(&s_syncOkToPrintMutex);
}

//Shuts down threads
void Output_shutdown()
{
    pthread_cancel(threadOutput);
    pthread_join(threadOutput, NULL);
}

//Outputs messages that are put on list
void *Output_outputThread(void* unused)
{
    while(1){
        //Critical section
        pthread_mutex_lock(&s_syncOkToPrintMutex);
        {   
            //If there are no messages on the list wait
            if(List_count(list) == 0){
                pthread_cond_wait(&s_syncOkToPrintCond, &s_syncOkToPrintMutex);
            }
            //take the first message of the list and output it
            printf("> %s", (char*) List_remove(list));
            //Signal blocking calls (if nextmessage was waitinf cause of list full)
            pthread_cond_signal(&s_syncOkToPrintCond);
           
        }
        pthread_mutex_unlock(&s_syncOkToPrintMutex);
    }
}