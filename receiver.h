#ifndef _RECEIVER_H_
#define _RECEIVER_H_

#define MAX_LEN 1024

void Receiver_init(List* receive_list, int socketDescriptor, bool* isDone);
void Receiver_shutdown();
void* Receiver_receiveThread(void*);

#endif
