#ifndef _OUTPUT_H_
#define _OUTPUT_H_

void Output_init(List* receive_list);
void Output_nextMessage(char* messageRx);
void Output_shutdown();
void *Output_outputThread(void* unused);

#endif
