#ifndef _INPUT_H_
#define _INPUT_H_

#include "list.h"
#include <pthread.h>
#include <stdbool.h> 

#define MAX_LEN_STR 1024

void Input_input_msg_producer_init(List* outputMsgList, bool* isDone);
void Input_msg_producer_ready_signal();
void Input_msg_producer_ready_wait();
char* Input_nextMsg();
void Input_shutdown();


#endif