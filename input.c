#include "input.h"
#include <stdio.h> 
#include <string.h> 
#include <stdlib.h>



static pthread_t threadInputProducer;
static List* outgoingMsgList;
static char* message;
static bool* shouldEnd;

static pthread_cond_t outgoingMsgCondVar = PTHREAD_COND_INITIALIZER;
static pthread_mutex_t outgoingMsgMutex = PTHREAD_MUTEX_INITIALIZER;

//Takes messages from the keyboard and adds to list
void* Input_input_msg_producer( void* unused){ 

    char* newText = malloc((MAX_LEN_STR) * sizeof(char)); 


    while(1){ 
        
        fgets(newText, MAX_LEN_STR, stdin);
        pthread_mutex_lock(&outgoingMsgMutex); 
        {   
            //If we dont have any space in the list then wait
            if(List_count(outgoingMsgList) == LIST_MAX_NUM_NODES){
                pthread_cond_wait(&outgoingMsgCondVar, &outgoingMsgMutex);
            }
            //append msg to the end of list 
            List_append(outgoingMsgList, newText); 

            //make current Item the first Item in the list so when you try to List_remove(pList) you can get the FIFO item 
            List_first(outgoingMsgList);
            //Signals that the list has one less item (If sender is waiting for a new item on the list)
            pthread_cond_signal(&outgoingMsgCondVar);
        }
        pthread_mutex_unlock(&outgoingMsgMutex);
        
        //If ! is sent then close down
        if(newText[0] == '!'){
            break;
        }
        
    } 
    //Close the while loop in main and send shutdown signals to all the threads
    *shouldEnd = true;
    return 0;
}  

//Takes the first item from the list and returns it so that it can be sent (in sender.c)
char* Input_nextMsg(){
    //Critical Section
    pthread_mutex_lock(&outgoingMsgMutex);
    {
        //If there are no messages to send then block
        if(List_count(outgoingMsgList) == 0){
            pthread_cond_wait(&outgoingMsgCondVar, &outgoingMsgMutex);
        }
        //Get the first message from the list
        message = List_remove(outgoingMsgList);
        //If there are any blocking calls then signal them (from the input if the list is full)
        pthread_cond_signal(&outgoingMsgCondVar);
    }
    pthread_mutex_unlock(&outgoingMsgMutex);
    //return the message to be sent
    return message;
}

//Shuts down the thread
void Input_shutdown()
{
    pthread_cancel(threadInputProducer);
    pthread_join(threadInputProducer, NULL);
}


void Input_input_msg_producer_init(List* pOutputMsgList, bool* isDone){
    outgoingMsgList = pOutputMsgList;
    shouldEnd = isDone;
    pthread_create(&threadInputProducer, NULL, Input_input_msg_producer, NULL);
}

