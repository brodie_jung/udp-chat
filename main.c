#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netdb.h>
#include <unistd.h>
#include "list.h"
#include "input.h"
#include "sender.h"
#include "output.h"
#include "receiver.h"



int main(int argc, char ** args)
{
    printf("************* Starting *************\n");

    if(argc != 4){
        fprintf(stderr, 
            "Usage: %s [my port number] [remote machine name] [remote port number] \n",
            args[0]);
        exit(EXIT_FAILURE);
    }

    //Setting up variables for lists and ports
    List* outgoingMsgList = List_create();
    List* receive_list = List_create();
    unsigned short sourcePortNum = atoi(args[1]);
    bool shouldEnd = false;

    //Setting up socket
    struct sockaddr_in sin; 
    memset(&sin, 0, sizeof(sin)); 
    sin.sin_family = AF_INET; 
    sin.sin_addr.s_addr = htonl(INADDR_ANY); 
    sin.sin_port = htons(sourcePortNum); 
    int socketDescriptor = socket(PF_INET, SOCK_DGRAM, 0);

    //If the socket doesnt bind then give error and exit
    if(
        bind(socketDescriptor, (struct sockaddr*) &sin, sizeof(sin)) != 0
    ){
        fprintf(stderr, "couldn't bind socket");
        exit(EXIT_FAILURE);
    }

    // All of these methods run in parallel in four separate threads

    // Items of the nodes are input strings
    Input_input_msg_producer_init(outgoingMsgList, &shouldEnd); 
    
    // args[2] is ip address and args[3] is port number of dest.
    Sender_msg_sender_consumer_init(outgoingMsgList, socketDescriptor, args[2], args[3]); 
    // Consume, meaning display, the messages in the list.

    
    
    //Receiver listens on the socket then adds to end of list
    //shouldEnd is a pointer to the bool that decides of the while loop below runs or we want to shutdown the threads
    Receiver_init(receive_list, socketDescriptor, &shouldEnd);
    //Outputs whatever is at the front of the list to the screen
    Output_init(receive_list);

    //shouldEnd only changes is ! is inputted
    while(!shouldEnd);

    //Shutdown Module
    Receiver_shutdown();
    Output_shutdown();
    Input_shutdown();
    Sender_shutdown();
    close(socketDescriptor);

    printf("************* done *************\n");
    return 0;
}

    