
CC = gcc
CFLAGS = -Wall -g -std=c99 -D _POSIX_C_SOURCE=200809L -Werror
DEPS = main.c receiver.c output.c
OBJ = main.o receiver.o output.o list.o

all: 
	gcc -o s-talk list.o main.c receiver.c output.c sender.c input.c -lpthread

build:
	gcc -o s-talk -Wall -g -c main.c receiver.c output.c -lpthread

clean:
	rm s-talk