#include <stdio.h>
#include <stdlib.h>
#include <netdb.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>
#include "list.h"
#include "output.h"
#include "receiver.h"


static pthread_t threadPID;
static int socketDescriptor;
static List* list;
static int socketDesc;
static bool* shouldEnd;


void* Receiver_receiveThread(void* unused)
{   
    while(1)
    {
        //Read
        //Listens on the socket for any messages
        struct sockaddr_in sinRemote;
        unsigned int sin_len = sizeof(sinRemote);

        char messageRx[MAX_LEN];


        int bytesRx = recvfrom(socketDesc,messageRx, MAX_LEN, 0, (struct sockaddr*)&sinRemote,&sin_len);



        int terminateldx = (bytesRx < MAX_LEN) ? bytesRx: MAX_LEN -1; 
        messageRx[terminateldx] = 0;
        long remotePort = ntohs(sinRemote.sin_port);
        //If a message comes in add it to the end of the list
        if(messageRx != NULL){
            Output_nextMessage(messageRx);
        }
        if(messageRx[0] == '!'){
            break;
        }
    }
    *shouldEnd = true;
    return NULL;
}


//All based off code from Workshop
void Receiver_init(List* receive_list, int socketDescriptor, bool* isDone)
{
    //Gets socket and list then starts thread
    list = receive_list;
    shouldEnd = isDone;
    socketDesc = socketDescriptor;
    pthread_create(&threadPID,NULL, Receiver_receiveThread,NULL);
}

//Shuts down thread
void Receiver_shutdown()
{
    //Cancel the thread
    pthread_cancel(threadPID);

    //Wait for thread to finsih
    pthread_join(threadPID, NULL);
}